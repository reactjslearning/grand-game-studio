import axios from 'axios'
const TemplateService = {
    create: (data) => {
        return axios.post('api/Templates', data)
    },
    get: () => {
        return axios.get('api/Templates')
    },
}

export default TemplateService;