import axios from 'axios'
const AccountService = {
    create: (data) => {
        return axios.post('api/Accounts', data)
    },
    getAll: () => {
        return axios.get("api/Accounts");
    },
    edit: (id, data) => {
        return axios.put(`api/Accounts/${id}`, data);
    },
    remove: (id) => {
        return axios.delete(`api/Accounts/${id}`);
    },
   
}

export default AccountService;