import axios from 'axios'
const GameService = {
    create: (data) => {
        return axios.post('api/Games', data)
    },
    getAll: () => {
        return axios.get("api/Games");
    },
    get: (id) => {
        return axios.get(`api/Games/${id}`);
    },
    edit: (id, data) => {
        return axios.put(`api/Games/${id}`, data);
    },
    remove: (id) => {
        return axios.delete(`api/Games/${id}`);
    },
    getByAccount:(id)=>{
        return axios.get(`api/Games/GetGameByAccount/${id}`);
    }
}

export default GameService;