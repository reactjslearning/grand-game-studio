import React, { Component } from 'react';
import Navbar from './components/navbar';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Switch, Route } from 'react-router-dom'
import AccountHome from './components/account/home';
import { Box } from '@material-ui/core';
import GameHome from './components/game/home';
import TemplateHome from './components/template/home';
export default class App extends Component {
  render() {
    return (
      <React.Fragment>
        <CssBaseline />
        <Navbar />
        <Box mt={5}>
          <Switch>
            <Route  path="/account" component={AccountHome} />
            <Route  path="/games" component={GameHome} />
            <Route  path="/template" component={TemplateHome} />
            <Route exact path="/" component={AccountHome} />
          </Switch>
        </Box>
      </React.Fragment>
    );
  }
}
