import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom'
import App from './App';
import axios from 'axios'


import registerServiceWorker from './registerServiceWorker';
const rootElement = document.getElementById('root');

// axios.defaults.baseURL = 'https://localhost:5001/'

ReactDOM.render(
  <Router>
    <App />
  </Router>,
  rootElement);

registerServiceWorker();
