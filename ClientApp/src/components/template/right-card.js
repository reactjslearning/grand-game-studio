import React, { useRef } from 'react'
import { useDrag, useDrop } from 'react-dnd'
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { Checkbox, ListItem, ListItemAvatar, Avatar } from '@material-ui/core';


const style = {
  border: '1px dashed gray',
  marginBottom: '2px',
  backgroundColor: 'white',
  cursor: 'move',
}
const RightCard = ({ game, index, moveCard, handleToggle, checked }) => {
  const ref = useRef(null)
  const [, drop] = useDrop({
    accept: 'gameCard',
    hover(item, monitor) {
      if (!ref.current) {
        return
      }
      const dragIndex = item.index
      const hoverIndex = index
      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return
      }
      // Determine rectangle on screen
      const hoverBoundingRect = ref.current.getBoundingClientRect()
      // Get vertical middle
      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2
      // Determine mouse position
      const clientOffset = monitor.getClientOffset()
      // Get pixels to the top
      const hoverClientY = clientOffset.y - hoverBoundingRect.top
      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%
      // Dragging downwards
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return
      }
      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return
      }
      // Time to actually perform the action
      moveCard(dragIndex, hoverIndex)
      // Note: we're mutating the monitor item here!
      // Generally it's better to avoid mutations,
      // but it's good here for the sake of performance
      // to avoid expensive index searches.
      item.index = hoverIndex
    },
  })
  const [{ isDragging }, drag] = useDrag({
    item: { type: 'gameCard', id: game.id, index },
    collect: monitor => ({
      isDragging: monitor.isDragging(),
    }),
  })
  const opacity = isDragging ? 0 : 1
  drag(drop(ref))
  return (
    <ListItem ref={ref} role="listitem " onClick={() => handleToggle(game)} style={{ opacity, cursor: 'move' }}>
      <ListItemIcon>
        <Checkbox
          tabIndex={-1}
          disableRipple
          checked={checked}
        />
      </ListItemIcon>
      <ListItemAvatar>
        <Avatar alt="Image Icon" src={`/images/${game.icon}`} />
      </ListItemAvatar>
      <ListItemText primary={game.gameName} secondary={game.account} />
    </ListItem>
  )
}
export default RightCard



