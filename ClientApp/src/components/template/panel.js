
import React from 'react'
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Checkbox from '@material-ui/core/Checkbox';
import Divider from '@material-ui/core/Divider';
import { DndProvider } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import LeftCard from './left-card';
import RightCard from './right-card';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { Skeleton } from '@material-ui/lab';
import { Box } from '@material-ui/core';

const PanelBox = ({ position, games, moveCard, checkedList, handleToggle, handleToggleAll, allItemChecked }) => {
    return (
        <Card>
            <CardHeader
                avatar={
                    <Checkbox
                        onClick={() => handleToggleAll(checkedList, position)}
                        checked={allItemChecked(checkedList, position)} />
                }
                title={position === 'left' ? 'All available games' : 'Move here the games and sort out for template'}
                subheader={`${checkedList.length} selected`}
            />
            <Divider />
            <List dense component="div" role="list">
                {position === 'left' && (
                    <React.Fragment>
                        {games.map((item) => {
                            const exist = checkedList.find(x => x.id === item.id);
                            const flag = exist ? true : false;
                            return (
                                <LeftCard key={Math.random()} checked={flag} handleToggle={handleToggle} game={item} />
                            )
                        })}
                        {games.length < 1 && [1, 2, 3, 4].map((item) => {
                            return (
                                <Box key={Math.random()} display="flex" m={1} px={2}>
                                    <Skeleton variant="circle" width={25} height={25} />
                                    <Box ml={2} clone>
                                        <Skeleton variant="rect" width="90%" height={25} />
                                    </Box>
                                </Box>
                            )
                        })}
                    </React.Fragment>

                )}
                {position === 'right' && (
                    <React.Fragment>
                        <DndProvider backend={HTML5Backend}>
                            {games.map((item, index) => {
                                const exist = checkedList.find(x => x.id === item.id);
                                const flag = exist ? true : false;
                                return (
                                    <RightCard key={Math.random()} checked={flag} handleToggle={handleToggle} game={item} index={index} moveCard={moveCard} />
                                )
                            })}
                        </DndProvider>
                        {games.length < 1 && [1, 2, 3, 4].map((item) => {
                            return (
                                <Box display="flex" m={1} px={2} key={Math.random()}>
                                    <Skeleton variant="circle" width={25} height={25} />
                                    <Box ml={2} clone>
                                        <Skeleton variant="rect" width="90%" height={25} />
                                    </Box>
                                </Box>
                            )
                        })}
                    </React.Fragment>

                )}
                <ListItem />
            </List>

        </Card>
    )
}

export default PanelBox
