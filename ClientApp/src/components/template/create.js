
import React, { useEffect, useState } from 'react'
import { Box, Typography } from '@material-ui/core'

import AccountService from '../../services/account';
import TransferBox from './transferBox';
import GameService from '../../services/game';
import Select from 'react-select';
import TemplateService from '../../services/template';
const CreateTemplate = () => {
    const [accounts, setAccounts] = useState([]);
    const [gamesList, setgamesList] = useState([]);
    const [template, setTemplate] = useState([]);

    useEffect(() => {
        let mounted = true;
        AccountService.getAll()
            .then((response) => {
                if (mounted) {
                    const mapped = response.data.map((item) => {
                        return {
                            value: item.id,
                            label: item.name
                        }
                    })
                    setAccounts(mapped);
                }
            })
        TemplateService.get()
            .then((response) => {
                if (mounted) {
                    const mapped = response.data.map((item) => {
                        return {
                            ...item,
                            templateId: item.id
                        }
                    })
                    setTemplate(mapped)
                }
            })
        return () => { mounted = false }
    }, []);

    const handleChange = (event) => {
        getGames(event);
    }
    const getGames = (event) => {
        if (!event) {
            setgamesList([]);
            return
        };
        const promiseList = [];
        event.forEach(item => {
            const promise = GameService.getByAccount(item.value);
            promiseList.push(promise)
        });
        Promise.all(promiseList).then(function (responseList) {
            let array = [];
            responseList.forEach((response) => {
                array = [...array, ...response.data]
            })
            setgamesList(array)
        });
    }
    return (
        <div>
            <Box py={2}>
                <Typography variant="h5">Create Template</Typography>
            </Box>
            {accounts.length > 0 && (
                <Select
                    onChange={handleChange}
                    options={accounts}
                    isMulti
                    isSearchable
                    autoFocus
                />
            )}
            <br />
            <TransferBox gameList={gamesList} template={template} />
        </div>
    )
}

export default CreateTemplate
