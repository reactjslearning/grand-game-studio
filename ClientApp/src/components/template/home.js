
import React from 'react'
import { Box, Paper, } from '@material-ui/core'
import CreateTemplate from './create'

const TemplateHome = ({ match }) => {
    return (
        <React.Fragment>
            <Box display="flex" justifyContent="center">
                <Box width="70%" p={3} clone mb={6}>
                    <Paper >
                        <CreateTemplate />
                    </Paper>
                </Box>
            </Box>
        </React.Fragment>
    )
}

export default TemplateHome
