
import React from 'react'
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { Checkbox, ListItem, ListItemAvatar, Avatar } from '@material-ui/core';


const LeftCard = ({ game, handleToggle, checked }) => {
    return (
        <ListItem role="listitem" onClick={() => handleToggle(game)}>
            <ListItemIcon>
                <Checkbox
                    tabIndex={-1}
                    disableRipple
                    checked={checked}
                />
            </ListItemIcon>
            <ListItemAvatar>
                <Avatar alt="Image Icon" src={`/images/${game.icon}`} />
            </ListItemAvatar>
            <ListItemText primary={game.gameName} secondary={game.account} />
        </ListItem>
    )
}

export default LeftCard
