import React, { useState, useCallback, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import PanelBox from './panel';
import update from 'immutability-helper'
import TemplateService from '../../services/template';
import { Box, Chip } from '@material-ui/core';

export default function TransferBox({ gameList, template }) {
  const [left, setLeft] = useState([]);
  const [right, setRight] = useState([]);
  const [message, setMessage] = useState('')
  const [leftChecked, setLeftChecked] = useState([]);
  const [rightChecked, setRightChecked] = useState([]);

  useEffect(() => {

    if (gameList.length < 1) {
      setLeft([])
      setLeftChecked([])
      setRight([])
      setRightChecked([])
    } else {
      debugger
      const clone = [...gameList];
      right.forEach((item) => {
        const index = clone.findIndex((el) => el.id === item.id);
        if (index >= 0) {
          clone.splice(index, 1)
        }
      })
      setLeft(clone);
    }
    if (template && template.length > 0) {
      setRight(template)
      setRightChecked([])
    }


  }, [gameList, template]);


  const moveCard = useCallback(
    (dragIndex, hoverIndex) => {
      const dragCard = right[dragIndex]
      setRight(
        update(right, {
          $splice: [[dragIndex, 1], [hoverIndex, 0, dragCard]],
        }),
      )
    },
    [right],
  )
  const handleToggleLeft = (game) => {
    const index = leftChecked.findIndex((x) => x.id === game.id);
    const leftCheckedClone = [...leftChecked];
    if (index >= 0) {
      leftCheckedClone.splice(index, 1)
    } else {
      leftCheckedClone.push(game)
    }
    setLeftChecked(leftCheckedClone)
  }
  const handleToggleRight = (game) => {
    const index = rightChecked.findIndex((x) => x.id === game.id);
    const rightCheckedClone = [...rightChecked];
    if (index >= 0) {
      rightCheckedClone.splice(index, 1)
    } else {
      rightCheckedClone.push(game)
    }
    setRightChecked(rightCheckedClone)
  }
  const handleMoveRight = () => {
    const filtered = [];
    leftChecked.forEach(element => {
      const exist = right.find(x => x.id === element.id);
      if (!exist) {
        filtered.push(element);
      }
    });
    if (filtered.length > 0) {
      setMessage('');
    }
    setRight([...right, ...filtered]);
    const clone = [...left];
    left.forEach(element => {
      const checked = leftChecked.find(x => x.id === element.id);
      if (checked) {
        const index = clone.findIndex(x => x.id === checked.id);
        if (index >= 0) {
          clone.splice(index, 1);
        }
      }
    });
    setLeftChecked([]);
    setLeft(clone);
  }
  const handleMoveLeft = () => {
    setLeft([...left, ...rightChecked]);
    const clone = [...right]
    right.forEach(element => {
      const checked = rightChecked.find(x => x.id === element.id);
      if (checked) {
        const index = clone.findIndex(x => x.id === checked.id);
        if (index >= 0) {
          clone.splice(index, 1);
        }
      }
    });
    setRightChecked([]);
    setRight(clone);
  }
  const allItemChecked = (items, position) => {
    if (position === 'left') {
      return items.length === left.length && left.length > 0;
    } else {
      return items.length === right.length && right.length > 0;
    }
  }
  const handleToggleAll = (items, position) => {
    if (position === 'left') {
      if (left.length > 0 && items.length < left.length) {
        setLeftChecked([...left])
      } else {
        setLeftChecked([])
      }
    } else {
      if (right.length > 0 && items.length < right.length) {
        setRightChecked([...right])
      } else {
        setRightChecked([])
      }
    }
  }
  const onCreate = () => {
    if (right.length < 1) {
      setMessage('Select games to create template.');
      return;
    }
    const data = right.map((item) => {
      return {
        gameId: item.id
      }
    })
    TemplateService.create({ templates: data })
      .then((response) => {
        setMessage('Template saved.');
      })
  }
  const handleRemoveMessag = () => {
    setMessage('')
  }
  return (
    <React.Fragment>
      {message.trim().length > 0 && (
        <Box display="flex" m={2} justifyContent="center">
          <Chip
            onDelete={handleRemoveMessag}
            label={message}
            color="secondary"
          />
        </Box>
      )}
      <Grid container spacing={2} justify="center" alignItems="center">
        <Grid item sm={5}>
          <PanelBox position="left"
            games={left}
            checkedList={leftChecked}
            handleToggle={handleToggleLeft}
            allItemChecked={allItemChecked}
            handleToggleAll={handleToggleAll} />
        </Grid>
        <Grid item sm={2}>
          <Grid container direction="column" spacing={4} alignItems="center">
            <Grid item>
              <Button
                variant="outlined"
                size="small"
                aria-label="move selected right"
                onClick={handleMoveRight}
                disabled={leftChecked.length === 0}
              >
                &gt;
             </Button>
            </Grid>
            <Grid item>
              <Button
                variant="outlined"
                size="small"
                aria-label="move selected left"
                onClick={handleMoveLeft}
                disabled={rightChecked.length === 0}
              >
                &lt;
          </Button>
            </Grid>
          </Grid>
        </Grid>
        <Grid item sm={5}>
          <PanelBox position="right" moveCard={moveCard}
            games={right} checkedList={rightChecked}
            handleToggle={handleToggleRight}
            allItemChecked={allItemChecked}
            handleToggleAll={handleToggleAll} />
        </Grid>
      </Grid>
      <br />
      <Grid container justify="flex-end">
        <Grid item>
          <Button onClick={onCreate} type="button" variant="contained" color="primary" >
            Create Template
          </Button>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}