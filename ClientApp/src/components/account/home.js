import React, { useEffect, useState } from 'react'
import { Paper, Box, Divider } from '@material-ui/core';
import AccountList from './list';
import CreateAccount from './create';
import AccountService from '../../services/account'

const AccountHome = () => {
    const [accounts, setAccounts] = useState([]);
    const [account, setAccount] = useState(null);

    useEffect(() => {
        let mounted = true;
        AccountService.getAll()
            .then((response) => {
                if (mounted) {
                    setAccounts(response.data);
                }
            })
            return () => (mounted = false)
    }, [])
    const onSubmit = (data) => {
        createPromise(data)
            .then((response) => {
                if (data.id) {
                    const accountIndex = accounts.findIndex((item) => item.id === data.id);
                    const accountsClone = [...accounts];
                    accountsClone[accountIndex] = data;
                    setAccounts(accountsClone);
                } else {
                    setAccounts([response.data, ...accounts]);
                }
                setAccount(null);
            })
    }
    const createPromise = (data) => {
        return data.id ? AccountService.edit(data.id, data) : AccountService.create(data);
    }
    const onEditAccount = (id) => {
        const account = accounts.find((item) => item.id === id);
        setAccount(account);
    }
    const onDeleteAccount = (id) => {

        AccountService.remove(id)
            .then(() => {
                const accountIndex = accounts.findIndex((item) => item.id === id);
                const accountsClone = [...accounts];
                accountsClone.splice(accountIndex, 1)
                setAccounts(accountsClone);
            })
    }
    const onReset = () => {
        setAccount(null);
    }


    return (
        <Box display="flex" justifyContent="center">
            <Box height={600} width="60%" clone>
                <Paper >
                    <Box px={10} >
                        <CreateAccount reset={onReset} onSubmit={onSubmit} selectedAccount={account} />
                    </Box>
                    <Divider />
                    <Box px={10} >
                        <AccountList editAccount={onEditAccount} deleteAccount={onDeleteAccount} accounts={accounts} />
                    </Box>
                </Paper>
            </Box>

        </Box>
    )
}

export default AccountHome

