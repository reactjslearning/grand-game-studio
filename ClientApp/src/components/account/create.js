
import React, { useState, useEffect } from 'react'
import { Box, TextField, Button, Typography } from '@material-ui/core';

const CreateAccount = (props) => {
    const [name, setName] = useState("");
    const [error, setError] = useState(false)
    const [selectedAccount, setselectedAccount] = useState(null)
    const onSubmit = (e) => {
        e.preventDefault();

        if (name.trim().length > 0) {
            if (selectedAccount) {
                props.onSubmit({ id: selectedAccount.id, name: name });
            } else {
                props.onSubmit({ name: name });
            }
            setName("");
            setError(false)
            return;
        }
        setError(true);
    }
    const onChange = (e) => {
        setName(e.target.value);
        setError(false)
    }
    const onReset = () => {
        setselectedAccount(null);
        setName("");
        setError(false)
        props.reset();
    }
    useEffect(() => {
        if (props.selectedAccount) {
            setselectedAccount(props.selectedAccount);
            setName(props.selectedAccount.name)
        } else {
            setselectedAccount(null);
        }
    }, [props.selectedAccount])
    return (
        <Box p={2}>
            <Typography variant="h6" > Add Account</Typography>
            <form onSubmit={onSubmit}>
                <Box display="flex" mt={1}>
                    <Box flexGrow={2} >
                        <TextField
                            value={name}
                            onChange={onChange}
                            margin="dense"
                            autoFocus
                            fullWidth
                            error={error && name.trim().length < 1}
                            id="outlined-bare"
                            placeholder="Enter something"
                            variant="outlined"
                        />
                    </Box>

                    <Box ml={2} m={1} flexGrow={1} display="flex">
                        <Button fullWidth type="submit" variant="contained" color="primary" >
                            Save
                         </Button> &nbsp;
                       {selectedAccount && (
                            <Button onClick={onReset} fullWidth type="button" variant="contained" color="secondary" >
                                Clear
                         </Button>
                        )}
                    </Box>
                </Box>
            </form>
        </Box>
    )
}

export default CreateAccount
