import React from 'react'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import EditOutlined from '@material-ui/icons/EditOutlined';
import DeleteIcon from '@material-ui/icons/Delete';
import { Divider, Box } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';

const AccountList = ({ accounts, editAccount, deleteAccount }) => {
    return (
        <React.Fragment>
            <Box maxHeight={430} overflow="auto">
                 <br/>
                {accounts.map((item) => {
                    return (
                        <React.Fragment key={`accounts-${item.id}`}>
                            <List dense={true} >
                                <ListItem>
                                    <ListItemText
                                        primary={item.name}
                                    />
                                    <ListItemSecondaryAction>
                                        <IconButton onClick={() => editAccount(item.id)} edge="start" aria-label="delete">
                                            <EditOutlined />
                                        </IconButton>
                                        <IconButton onClick={() => deleteAccount(item.id)} edge="end" aria-label="delete">
                                            <DeleteIcon />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            </List>
                            <Divider variant="middle" />
                        </React.Fragment>
                    )
                })}
                {accounts.length < 1 && [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((item) => {
                    return (
                        <React.Fragment key={`skeleton-${item.id}`} >
                            <Box display="flex" pl={2} >
                                <Skeleton height={10} width="85%" /> &nbsp; &nbsp; &nbsp; &nbsp;
                                <Skeleton variant="circle" height={25} width={25} /> &nbsp; &nbsp;
                                <Skeleton variant="circle" height={25} width={25} />
                            </Box>
                        </React.Fragment>
                    )
                })}
            </Box>
        </React.Fragment>
    )
}

export default AccountList
