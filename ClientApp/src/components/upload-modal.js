import React, { useRef, useState } from 'react'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Button } from '@material-ui/core';
import { FilePond, registerPlugin } from "react-filepond";
import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation';
import FilePondPluginImagePreview from "filepond-plugin-image-preview";


// Register the plugins
registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview);

const UploadDialog = ({ open, handleClose, handleAdd }) => {
    let pond = useRef(null);
    const [file, setFile] = useState('')
    const onLoaded = response => {
        response = response.substring(1, response.length - 1);
        setFile(response)
    };
    const onAdd = () => {
        if (file.trim().length < 1) {
            return;
        }
        handleAdd(file);
        setFile('')
    }
    return (
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Upload file</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Upload your files and image url will be add to your input.
                </DialogContentText>
                <FilePond
                    ref={pond}
                    allowMultiple={false}
                    server={{
                        url: `/api/upload`,
                        process: {
                            onload: onLoaded
                        }
                    }}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Cancel
                </Button>
                <Button onClick={onAdd} color="primary">
                    Add
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default UploadDialog
