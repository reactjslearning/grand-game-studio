import React, { useState } from 'react'
import { Grid, TextField, Box, Typography, Button, Divider, Link, Chip, Switch, FormControlLabel } from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom'
import MenuItem from '@material-ui/core/MenuItem';
import "filepond/dist/filepond.min.css";
import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css";
import UploadDialog from '../upload-modal';

const initial = {
    accountId: '',
    name: '',
    link: '',
    image: '',
    icon: '',
    package: '',
    isInterstitialAd: false,
    interstitialImage:'',
    isBannerAdd: false,
    isActive: false
};

const GameForm = ({ onSubmit, accounts, game }) => {
    const [clickedProperty, setClickedProperty] = useState('');
    const [openDialog, setopenDialog] = useState(false);
    const [errorMessage, seterrorMessage] = useState('')
    const [form, setForm] = useState(game ? game : initial)
    const onSubmitHandler = (e) => {
        e.preventDefault();
        let flag = false;
        Object.values(form).forEach(value => {
            if (typeof value === 'number' || value === null) return;
            if (value.toString().trim().length < 1) {
                flag = true;
            }
        });
        if (flag) {
            seterrorMessage('Fill the required fields.');
            return;
        };
        onSubmit(form);
        setForm(initial);
    }
    const handleRemoveError = () => {
        seterrorMessage('')
    }
    const onChangeHandler = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
        seterrorMessage('')
    }
    const onSliderChangeHandler = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.checked
        })
        seterrorMessage('')
    }
    const handleAdd = (url) => {
        if (url.trim().length < 1) {
            return;
        }
        setopenDialog(false);
        setForm({
            ...form,
            [clickedProperty]: url
        })
        console.log(form)
    }
    return (
        <form noValidate autoComplete="off" onSubmit={onSubmitHandler}>
            <UploadDialog open={openDialog} handleAdd={(url) => handleAdd(url)} handleClose={() => setopenDialog(false)} />
            <Typography variant="h5" gutterBottom>
                {game ? 'Edit' : 'Create'} Game
            </Typography>
            {errorMessage.trim().length > 0 && (
                <Box display="flex" justifyContent="center">
                    <Chip
                        onDelete={handleRemoveError}
                        label={errorMessage}
                        color="secondary"
                    />
                </Box>
            )}

            <br />
            <Grid container spacing={2} >
                <Grid item sm={6}>
                    <TextField
                        fullWidth
                        id="accountId"
                        name="accountId"
                        select
                        InputLabelProps={{ shrink: true }}
                        value={form.accountId}
                        label="Account"
                        margin="normal"
                        variant="outlined"
                        onChange={onChangeHandler}
                    >
                        {accounts.map((item) => {
                            return (
                                <MenuItem id={`accounts-${new Date()}`} value={item.id}>{item.name}</MenuItem>
                            )
                        })}
                    </TextField>

                </Grid>
                <Grid item sm={6}>
                    <TextField
                        id="name"
                        name="name"
                        label="Name"
                        fullWidth
                        value={form.name}
                        margin="normal"
                        InputLabelProps={{ shrink: true }}
                        variant="outlined"
                        onChange={onChangeHandler}
                    />
                </Grid>
            </Grid>
            <br />
            <Grid container spacing={2}>
                <Grid item sm={6}>

                    <TextField
                        id="link"
                        name="link"
                        label="Link"
                        fullWidth
                        value={form.link}
                        margin="normal"
                        variant="outlined"
                        multiline
                        InputLabelProps={{ shrink: true }}
                        onChange={onChangeHandler}
                    />
                </Grid>
                <Grid item sm={6}>
                    <TextField
                        id="package"
                        name="package"
                        label="Package"
                        fullWidth
                        value={form.package}
                        margin="normal"
                        variant="outlined"
                        multiline
                        InputLabelProps={{ shrink: true }}
                        onChange={onChangeHandler}
                    />
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item sm={6}>
                    <TextField
                        id="icon"
                        name="icon"
                        label="Icon"
                        fullWidth
                        value={form.icon}
                        margin="normal"
                        variant="outlined"
                        multiline
                        InputLabelProps={{ shrink: true }}
                        onChange={onChangeHandler}
                        helperText={
                            <Link
                                component="a"
                                variant="body2"
                                onClick={() => {
                                    setopenDialog(true);
                                    setClickedProperty('icon')
                                }}
                            >
                                Add icon
                            </Link>
                        }
                    />
                </Grid>
                <Grid item sm={6}>
                    <TextField
                        id="image"
                        name="image"
                        label="Image"
                        fullWidth
                        margin="normal"
                        variant="outlined"
                        multiline
                        InputLabelProps={{ shrink: true }}
                        value={form.image}
                        onChange={onChangeHandler}
                        helperText={
                            <Link
                                component="a"
                                variant="body2"
                                onClick={() => {
                                    setopenDialog(true);
                                    setClickedProperty('image')
                                }}
                            >
                                Add image
                            </Link>
                        }
                    />
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item sm={6}>
                    <TextField
                        id="interstitialImage"
                        name="interstitialImage"
                        label="Interstitial Image"
                        fullWidth
                        value={form.interstitialImage}
                        margin="normal"
                        variant="outlined"
                        multiline
                        InputLabelProps={{ shrink: true }}
                        onChange={onChangeHandler}
                        helperText={
                            <Link
                                component="a"
                                variant="body2"
                                onClick={() => {
                                    setopenDialog(true);
                                    setClickedProperty('interstitialImage')
                                }}
                            >
                                Add Interstitial Image
                            </Link>
                        }
                    />
                </Grid>
                <Grid item sm={6}>
                   
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item sm={4}>
                    <FormControlLabel
                        name="isBannerAdd"
                        control={
                            <Switch
                                checked={form.isBannerAdd}
                                onChange={onSliderChangeHandler}
                                value={form.isBannerAdd} />
                        }
                        label="Banner Ad"
                    />
                </Grid>
                <Grid item sm={4}>
                    <FormControlLabel
                        name="isInterstitialAd"
                        control={
                            <Switch
                            checked={form.isInterstitialAd}
                                onChange={onSliderChangeHandler}
                                value={form.isInterstitialAd} />
                        }
                        label="Interstitial Ad"
                    />
                </Grid>
                <Grid item sm={4}>
                    <FormControlLabel
                        name="isActive"
                        control={
                            <Switch
                            checked={form.isActive}
                                onChange={onSliderChangeHandler}
                                value={form.isActive} />
                        }
                        label="Slider"
                    />
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item sm={12}>
                    <Box display="flex" justifyContent="flex-end" mt={2}>
                        <Button component={RouterLink} to="/games" variant="contained" color="primary" >
                            Cancel
                            </Button>
                        <Box ml={2} clone>
                            <Button type="submit" variant="contained" color="primary" >
                                Submit
                        </Button>
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        </form>
    )
}

export default GameForm
