
import React, { useState, useEffect } from 'react'
import { Box } from '@material-ui/core';

import AccountService from '../../services/account'
import GameService from '../../services/game';
import GameForm from './form';
import ConfirmationDialog from '../confirmation-dialog';


const CreateGame = () => {
    const [accounts, setAccounts] = useState([]);
    const [added, setAdded] = useState(false);
    useEffect(() => {
        let mounted = true;
        AccountService.getAll()
            .then((response) => {
                if (mounted) {
                    setAccounts(response.data);
                }
            })
        return () => (mounted = false)
    }, []);
    const onSubmit = (data) => {
        GameService.create(data)
            .then((response) => {
                setAdded(true)
            })
    }
    const handleClose = (data) => {
        setAdded(false)
    }

    return (
        <Box p={2} pt={4}>
            <ConfirmationDialog open={added} handleClose={handleClose} />
            <GameForm onSubmit={onSubmit} accounts={accounts} />
        </Box>
    )
}

export default CreateGame
