import React from 'react'
import { Skeleton } from '@material-ui/lab';
import { Box } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';


const GameSkeleton = () => {
    return (
        <Box width="33%" maxWidth={345} m={2} clone>
            <Card >
                <CardActionArea>
                    <Skeleton variant="rect" width="100%" height={150} />
                    <CardContent>
                        <Skeleton height={20} width="30%" />
                        <Skeleton height={6} width="100%" />
                        <Skeleton height={6} width="80%" />
                    </CardContent>
                </CardActionArea>
                <CardActions>
                        <Skeleton height={30} width="80%" />
                        <Skeleton height={30} width="80%" />
                </CardActions>
            </Card>
        </Box>
    )
}

export default GameSkeleton
