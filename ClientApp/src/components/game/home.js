import React from 'react'
import { Paper, Box, Divider } from '@material-ui/core';
import CreateGame from './create';
import { Route } from 'react-router-dom'
import GameList from './list';
import EditGame from './edit';
const GameHome = ({ match }) => {

    return (
        <Box display="flex" justifyContent="center">
            <Box width="80%" p={3} clone mb={6}>
                <Paper >
                    <Route path={`${match.path}/create`} component={CreateGame} />
                    <Route path={`${match.path}/list`} component={GameList} />
                    <Route path={`${match.path}/edit/:id`} component={EditGame} />
                    <Route exact path={`${match.path}`} component={GameList} />
                </Paper>
            </Box>

        </Box>
    )
}

export default GameHome

