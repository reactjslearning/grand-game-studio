import React, { useEffect, useState } from 'react'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';

import { Box, Typography, IconButton, Divider } from '@material-ui/core';
import AddCircleOutlineRoundedIcon from '@material-ui/icons/AddCircleOutlineRounded';
import { Link } from 'react-router-dom'
import GameService from '../../services/game';
import GameSkeleton from './skeleton';

const GameList = ({ match }) => {
    const [games, setGames] = useState([])
    useEffect(() => {
        let mounted = true;
        GameService.getAll()
            .then((response) => {
                if (mounted) {
                    setGames(response.data);
                }
            })
        return () => (mounted = false)
    }, []);

    const onDelete = (id) => {
        GameService.remove(id)
            .then(() => {
                const gameIndex = games.findIndex((item) => item.id === id);
                const gamesClone = [...games];
                gamesClone.splice(gameIndex, 1)
                setGames(gamesClone);
            })
    }

    return (
        <React.Fragment>
            <Box p={2} display="flex" justifyContent="space-between" >
                <Box mt={2} > <Typography variant="h5" >Games</Typography></Box>
                <IconButton component={Link} to={`${match.path}/create`}>
                    <AddCircleOutlineRoundedIcon fontSize="large" />
                </IconButton>
            </Box>
            <Box display="flex" flexWrap="wrap" >
                {games.map((item) => {
                    return (
                        <Box key={Math.random()} width={{ sm: '50%', md: '33%', xs: '100%' }} maxWidth={345} m={2} clone>
                            <Card >
                                <CardActionArea>
                                    <CardMedia
                                        style={{ height: 140 }}
                                        image={`/images/${item.icon}`}
                                        title="Contemplative Reptile"
                                    />
                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            {item.gameName}
                                        </Typography>
                                        <Divider />
                                        <Box display="flex" mt={1}>
                                            <strong>   Package:&nbsp;</strong>
                                            <Typography variant="" color="textSecondary" component="p">
                                                {item.packageName}
                                            </Typography>
                                        </Box>
                                        <Box display="flex" mt={1}>
                                            <strong>   Account:&nbsp;</strong>
                                            <Typography variant="" color="textSecondary" component="p">
                                                {item.account}
                                            </Typography>
                                        </Box>
                                    </CardContent>
                                </CardActionArea>
                                <CardActions>
                                    <Box width={1} display="flex" justifyContent="space-between">
                                        <Button component={Link} to={`${match.path}/edit/${item.id}`} size="small" color="primary">
                                            Edit
                                     </Button>
                                        <Button onClick={() => onDelete(item.id)} size="small" color="primary">
                                            Delete
                                    </Button>
                                    </Box>
                                </CardActions>
                            </Card>
                        </Box>
                    )
                })}

            </Box>
            {games.length < 1 && (
                <Box display="flex" flexWrap="wrap">
                    {[1, 2, 3, 4, 5, 6].map((item) => {
                        return (
                            <GameSkeleton />
                        )
                    })}
                </Box>
            )}

        </React.Fragment>
    )
}

export default GameList
