import React, { useState, useEffect } from 'react'
import { Box } from '@material-ui/core';

import AccountService from '../../services/account'
import GameService from '../../services/game';
import GameForm from './form';
import ConfirmationDialog from '../confirmation-dialog';


const EditGame = ({ match }) => {
    const [accounts, setAccounts] = useState([]);
    const [game, setGame] = useState(null);
    const [added, setAdded] = useState(false);
    useEffect(() => {
        getAccounts();
        getGame();
    }, []);
    const getAccounts = () => {
        AccountService.getAll()
            .then((response) => {
                setAccounts(response.data);
            })
    }
    const getGame = () => {
        debugger
        GameService.get(match.params.id)
            .then((response) => {
                setGame(response.data);
            })
    }

    const onSubmit = (data) => {
        GameService.edit(match.params.id, data)
            .then((response) => {
                setAdded(true)
            })
    }
    const handleClose = (data) => {
        setAdded(false)
    }
    return (
        <Box p={2} pt={4}>
            <ConfirmationDialog open={added} handleClose={handleClose} />
            {game ? (
                <GameForm game={game} onSubmit={onSubmit} accounts={accounts} />
            ) : ''}
        </Box>
    )
}

export default EditGame
