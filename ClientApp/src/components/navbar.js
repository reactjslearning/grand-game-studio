
import React from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { Box, } from '@material-ui/core';
import { Link } from 'react-router-dom'

const Navbar = () => {
    return (
        <AppBar position="static">
            <Toolbar>
                <IconButton edge="start" color="inherit" aria-label="menu">
                    <MenuIcon />
                </IconButton>
                <Box flexGrow={1} clone>
                    <Typography variant="h6" >
                        Grand Game Studio
                     </Typography>
                </Box>
                <Button component={Link} to="/account" color="inherit">Account</Button>
                <Button component={Link} to="/games" color="inherit">Games</Button>
                <Button component={Link} to="/template" color="inherit">Template</Button>
            </Toolbar>
        </AppBar>
    )
}

export default Navbar
