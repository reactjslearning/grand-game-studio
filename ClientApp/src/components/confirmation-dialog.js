
import React from 'react'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Link } from 'react-router-dom'
import { Button } from '@material-ui/core';

const ConfirmationDialog = ({ handleClose, open }) => {
    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{" Game added successfully."}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    Do you want to redirect to list ?
          </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button component={Link} to="/games" color="primary">
                    Redirect
                </Button>
                <Button color="primary" onClick={handleClose}>
                    Stay here
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default ConfirmationDialog
