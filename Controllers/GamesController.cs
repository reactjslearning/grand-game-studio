﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GrandGameApp.Model;

namespace GrandGameApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GamesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public GamesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Games
        [HttpGet]
        public async Task<IActionResult> GetGame()
        {
            var games = _context.Game;
            var gamesList = new List<Gamevm>();
            foreach (var x in games)
            {
                var vm = new Gamevm();
                var data = await _context.Account.FindAsync(x.AccountId);
                vm.GameName = x.Name;
                vm.Icon = x.icon;
                vm.LinkOfGame = x.link;
                vm.Image = x.image;
                vm.FullImage = x.interstitialImage;
                vm.Interstitial = x.isInterstitialAd;
                vm.BannerAd = x.isBannerAdd;
                vm.Slider = x.isActive;
                vm.PackageName = x.package;
                vm.Account = data.Name;
                vm.Id = x.Id;
                gamesList.Add(vm);
            }

            return Ok(gamesList);
        }

        // GET: api/Games/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetGame([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var game = await _context.Game.FindAsync(id);

            if (game == null)
            {
                return NotFound();
            }

            return Ok(game);
        }
        // GET: api/Games/5
        [HttpGet("GetGameByAccount/{id}")]
        public async Task<IActionResult> GetGameByAccount([FromRoute] int id)
        {
            var games = await _context.Game.Where(x => x.AccountId == id).ToListAsync();

            if (games == null)
            {
                return NotFound();
            }

            var gamesList = new List<Gamevm>();
            foreach (var x in games)
            {
                var vm = new Gamevm();
                var data = await _context.Account.FindAsync(x.AccountId);
                vm.GameName = x.Name;
                vm.Icon = x.icon;
                vm.LinkOfGame = x.link;
                vm.Image = x.image;
                vm.FullImage = x.interstitialImage;
                vm.Interstitial = x.isInterstitialAd;
                vm.BannerAd = x.isBannerAdd;
                vm.Slider = x.isActive;
                vm.PackageName = x.package;
                vm.Account = data.Name;
                vm.Id = x.Id;
                gamesList.Add(vm);
            }

            return Ok(gamesList);
        }
        // PUT: api/Games/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGame([FromRoute] int id, [FromBody] Game game)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != game.Id)
            {
                return BadRequest();
            }

            _context.Entry(game).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GameExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Games
        [HttpPost]
        public async Task<IActionResult> PostGame([FromBody] Game game)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Game.Add(game);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGame", new { id = game.Id }, game);
        }

        // DELETE: api/Games/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGame([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var template = _context.Template.FirstOrDefault(x => x.GameId == id);
          
            var game = await _context.Game.FindAsync(id);
            if (game == null )
            {
                return NotFound();
            }
            if (template != null)
            {
                _context.Template.Remove(template);
                await _context.SaveChangesAsync();
            }


            _context.Game.Remove(game);
            await _context.SaveChangesAsync();

            return Ok(game);
        }

        private bool GameExists(int id)
        {
            return _context.Game.Any(e => e.Id == id);
        }
    }
}