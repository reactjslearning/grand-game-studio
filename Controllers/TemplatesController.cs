﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GrandGameApp.Model;

namespace GrandGameApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TemplatesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public TemplatesController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult>  Gettemplate()
        {
            var games = _context.Template.Include(x => x.Game).OrderBy(x => x.Id);
            var gamesList = new List<Gamevm>();
            foreach (var x in games)
            {
                var vm = new Gamevm();
                var data = await _context.Account.FindAsync(x.Game.AccountId);
                vm.GameName = x.Game.Name;
                vm.Icon = x.Game.icon;
                vm.LinkOfGame = x.Game.link;
                vm.Image = x.Game.image;
                vm.FullImage = x.Game.interstitialImage;
                vm.Interstitial = x.Game.isInterstitialAd;
                vm.BannerAd = x.Game.isBannerAdd;
                vm.Slider = x.Game.isActive;
                vm.PackageName = x.Game.package;
                vm.Account = data.Name;
                vm.Id = x.Game.Id;
                gamesList.Add(vm);
            }
            return Ok(gamesList);
        }

        [HttpGet("GetGeneratedTemplate")]
        public async Task<IActionResult> GetGeneratedTemplate()
        {
            var games = _context.Template.Include(x => x.Game).OrderBy(x => x.Id);
            var gamesList = new List<GameDTO>();
            foreach (var x in games)
            {
                GameDTO vm = new GameDTO();
                var data = await _context.Account.FindAsync(x.Game.AccountId);
                vm.GameName = x.Game.Name;
                vm.Icon = x.Game.icon;
                vm.LinkOfGame = x.Game.link;
                vm.Image = x.Game.image;
                vm.FullImage = x.Game.interstitialImage;
                vm.Interstitial = x.Game.isInterstitialAd;
                vm.BannerAd = x.Game.isBannerAdd;
                vm.Slider = x.Game.isActive;
                vm.PackageName = x.Game.package;
                vm.Account = data.Name;
                vm.Id = x.Game.Id;
                gamesList.Add(vm);
            }
            return Ok(gamesList);
        }
        // GET: api/Templates/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Gettemplate([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var template = await _context.Template.FindAsync(id);

            if (template == null)
            {
                return NotFound();
            }

            return Ok(template);
        }

        // PUT: api/Templates/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Puttemplate([FromRoute] int id, [FromBody] Template template)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != template.Id)
            {
                return BadRequest();
            }

            _context.Entry(template).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!templateExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Templates
        [HttpPost]
        public async Task<IActionResult> Posttemplate([FromBody] TemplateVM data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _context.Database.ExecuteSqlCommand("TRUNCATE TABLE Template");
            _context.Template.AddRange(data.Templates);
            await _context.SaveChangesAsync();

            return Ok("Succeed");
        }
        [HttpPost("Updatetemplate")]
        public async Task<IActionResult> Updatetemplate([FromBody] TemplateVM data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _context.Database.ExecuteSqlCommand("TRUNCATE TABLE Template");
            _context.Template.AddRange(data.Templates);
            await _context.SaveChangesAsync();

            return Ok("Succeed");
        }
        // DELETE: api/Templates/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Deletetemplate([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var template = await _context.Template.FindAsync(id);
            if (template == null)
            {
                return NotFound();
            }

            _context.Template.Remove(template);
            await _context.SaveChangesAsync();

            return Ok(template);
        }

        private bool templateExists(int id)
        {
            return _context.Template.Any(e => e.Id == id);
        }
    }
}