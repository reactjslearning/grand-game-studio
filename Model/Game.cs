﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace GrandGameApp.Model
{
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string icon { get; set; }
        public string link { get; set; }
        public string image { get; set; }
        public string package { get; set; }
        public string interstitialImage { get; set; }
        public bool isBannerAdd { get; set; }
        public bool isInterstitialAd { get; set; }
        public bool isActive { get; set; }
        public int AccountId { get; set; }
        public Account Account { get; set; }
    }
    public class Gamevm
    {
        public int Id { get; set; }

        public string GameName { get; set; }
        public string Icon { get; set; }
        public string LinkOfGame { get; set; }
        public string Image { get; set; }
        public string FullImage { get; set; }
        public bool Interstitial { get; set; }
        public bool BannerAd { get; set; }
        public bool Slider { get; set; }
        public string PackageName { get; set; }
        public string Account { get; set; }
    }

    public class GameDTO
    {
        public int Id { get; set; }

        [JsonProperty(PropertyName = "GameName")]
        public string GameName { get; set; }
        [JsonProperty(PropertyName = "icon")]
        public string Icon { get; set; }
        [JsonProperty(PropertyName = "linkofgame")]
        public string LinkOfGame { get; set; }
        [JsonProperty(PropertyName = "image")]
        public string Image { get; set; }
        [JsonProperty(PropertyName = "fullimage")]
        public string FullImage { get; set; }
        [JsonProperty(PropertyName = "Interstitial")]
        public bool Interstitial { get; set; }
        [JsonProperty(PropertyName = "BannerAd")]
        public bool BannerAd { get; set; }
        [JsonProperty(PropertyName = "Slider")]
        public bool Slider { get; set; }
        [JsonProperty(PropertyName = "packagename")]

        public string PackageName { get; set; }
        [JsonProperty(PropertyName = "Account")]
        public string Account { get; set; }
    }

}
