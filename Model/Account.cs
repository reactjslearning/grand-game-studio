﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrandGameApp.Model
{
    public class Account
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Game> Game { get; set; }
    }
}
