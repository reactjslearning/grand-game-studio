﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrandGameApp.Model
{
    public class TemplateVM
    {
        public List<Template> Templates { get; set; }
    }
}
